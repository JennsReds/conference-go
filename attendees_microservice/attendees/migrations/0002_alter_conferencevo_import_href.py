# Generated by Django 4.0.3 on 2023-05-15 22:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conferencevo',
            name='import_href',
            field=models.CharField(max_length=50),
        ),
    ]
