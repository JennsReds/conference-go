from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # https://api.pexels.com/v1/search?query=CITY query=& STATE & per_page=#
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city}, {state}"
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {
            "picture_url": content["photos"][0]["src"]["original"]
        }
    except(KeyError, IndexError):
        return {
            "picture_url": None
        }


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    parameters = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, params=parameters)
    content = json.loads(response.content)

    try:
        latitutde = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

# long / lat  weather description
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitutde,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",

    }
    response = requests.get(url, params=params)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"]

    }
